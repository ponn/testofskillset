using Task1;
using Xunit;

namespace Task1Tests
{
    public class MultipleFinderTests
    {
        private readonly MultipleFinder _multipleFinder;

        public MultipleFinderTests()
        {
            _multipleFinder = new MultipleFinder();
        }

        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 0)]
        [InlineData(2, 0)]
        [InlineData(3, 3)]
        [InlineData(4, 3)]
        [InlineData(5, 8)]
        [InlineData(6, 14)]
        [InlineData(7, 14)]
        [InlineData(8, 14)]
        [InlineData(9, 23)]
        [InlineData(10, 33)]
        [InlineData(11, 33)]
        [InlineData(12, 45)]
        [InlineData(13, 45)]
        [InlineData(14, 45)]
        [InlineData(15, 60)]
        public void ShouldReturnTheSumOfAllMultiplesInvolving3And5BasedOnInputNumber(int upperLimit, int expectedSumOfMultiples)
        {
            var sumOfMultiples = _multipleFinder.FindSumOfMultiplesInvolving3And5(upperLimit);
            Assert.Equal(sumOfMultiples, expectedSumOfMultiples);
        }
    }
}
