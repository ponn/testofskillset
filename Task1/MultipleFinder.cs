﻿using System;

namespace Task1
{
    public class MultipleFinder
    {
        public int FindSumOfMultiplesInvolving3And5(int upperLimit)
        {
            var sumOfAllMultiplesInvolving3And5 = 0;
            for (var i = 0; i <= upperLimit; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    sumOfAllMultiplesInvolving3And5 += i;
                }
            }
            return sumOfAllMultiplesInvolving3And5;
        }
    }
}
